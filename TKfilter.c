// 与えられた画像の特徴点を検出し，それを特徴点の高い順に配列に格納するプログラム．

#include"lsq.c"

void ImageFeature(Matrix*im2,Image*im){
  int x,y,u,v,W=7,ix,iy;
  for(y=W+1;y<im->H-W-1;y++) for(x=W+1;x<im->W-W-1;x++){
    double ixx,ixy,iyy;
    ixx=iyy=ixy=0;
    for(v=-W;v<=W;v++) for(u=-W;u<=W;u++){
      ix=IElem(im, x+u+1, y+v, 1) - IElem(im, x+u-1, y+v, 1);
      ixx+=ix*ix;
      iy=IElem(im, x+v, y+u+1, 1) - IElem(im, x+v, y+u-1, 1);
      iyy+=iy*iy;
      ixy+=ix*iy;
    }
    DElem(im2,x,y)=((ixx+iyy)-sqrt((ixx+iyy)*(ixx+iyy)-4*(ixx*iyy-ixy*ixy)))/2;
  }
}

void sort_insertion(int a[][2],double N[],int n,int t){
  int i,j,t0,t1,t2;
  for(i=t;i<n;i++){
    t0=a[i][0]; t1=a[i][1]; t2=N[i];
    j=i;
    for(;j>=1 && N[j-1]<t2; j--){
     a[j][0]=a[j-1][0];
     a[j][1]=a[j-1][1];
     N[j]=N[j-1];
    }
    a[j][0]=t0; a[j][1]=t1,N[j]=t2;
  }
}

int MatrixLocalMax(int w[][2],double N[], Matrix*im2, int p){
  int x,y,u,v,W=7,n=0,a;
  for(y=W+1;y<im2->H-W-1;y++) for(x=W+1;x<im2->W-W-1;x++){
    double max=-1;
    for(v=-W;v<=W;v++) for(u=-W;u<=W;u++){
	if(max < DElem(im2,x+u,y+v)){
	  max = DElem(im2,x+u,y+v);
	}
    }
    if(max == DElem(im2,x,y)){
      if(n<p) a=n;
      if(n<p || DElem(im2,x,y)>N[p-1]){
      w[a][0]=x; w[a][1]=y; N[a]=DElem(im2,x,y);
      sort_insertion(w,N,a+1,a);
      }
      n++;
    }
  }
  return n; // 記録した点の数
}

void TKfilter(Image*im,int x[][2]){
  Matrix*im2;
  int kw;
  double N[9999];

  im2=MatrixAlloc(im->H,im->W);
  ImageFeature(im2,im);
  kw=MatrixLocalMax(x,N,im2,FeaturePoint);
}
