#include "image.h"


Image*ImageAlloc(int W,int H){
  Image*im=(Image*)malloc(sizeof(Image));
  im->W=W;
  im->H=H;
  im->data=(unsigned char*)malloc(W*H*3);
  return im;
}

Image*ImageRead(const char *name){
  int W,H;
  Image*im;
  char cmd[256];
  sprintf(cmd,"djpeg %s",name);
  FILE *fp=popen(cmd,"r");
  fscanf(fp,"%*s%d%d%*s%*c",&W,&H);
  im=ImageAlloc(W,H);
  fread(im->data,1,W*H*3,fp);
   pclose(fp);
  return im;
}

void ImageWrite(const char*name,Image*im){
  FILE *fp=fopen(name,"w");
  fprintf(fp,"P6 %d %d 255\n",im->W,im->H);
  fwrite(im->data,1,im->W*im->H*3,fp);
  fclose(fp);
}

void ImageClear(Image*im){
  int x,y;
  for(y=0;y<im->H;y++)
    for(x=0;x<im->W;x++){
      IElem(im,x,y,0)=0;
      IElem(im,x,y,1)=0;
      IElem(im,x,y,2)=0;
    }
}
