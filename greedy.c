// 2つの画像の特徴点を，それぞれ高い順に並べた2つの配列から，SSD表を作成し，4点の自動対応付けを行うプログラム．各々の4点から変換行列を導き出し，最も信頼度の高い行列を，与えられた配列に格納する．

double ImageSSD(Image*im,int x1,int y1, Image*im2,int x2,int y2){
  int i,j,W=7;
  double sr=0,sg=0,sb=0,dr,dg,db;
  for(i=-W;i<=W;i++) for(j=-W;j<=W;j++){
    dr  = IElem(im, x1+j, y1+i, 0) - IElem(im2, x2+j , y2+i, 0);
    dg  = IElem(im, x1+j, y1+i, 1) - IElem(im2, x2+j , y2+i, 1);
    db  = IElem(im, x1+j, y1+i, 2) - IElem(im2, x2+j , y2+i, 2);
    sr += dr*dr;
    sg += dg*dg;
    sb += db*db;
  }
  return sr+sg+sb;
}


void calcSSDtable(Matrix*mt,
		  Image*im ,int x1[][2],int N1,
		  Image*im2,int x2[][2],int N2){
  int i,j;
  for(i=0;i<N1;i++)
    for(j=0;j<N2;j++)
      Elem(mt,i,j) = ImageSSD(im ,x1[i][0],x1[i][1],
			      im2,x2[j][0],x2[j][1]);
}

int matchMethod(int w[][4],int match[][2],Matrix*mt,
                 Image*im ,int x1[][2],int N1,
                 Image*im2,int x2[][2],int N2){
  int i,j,k,il,jl,ii,ji,n=0;

  for(k=0;k<N1 && k<N2;k++){
    double sm=INFINITY,t;
    for(i=0;i<N1;i++){
      for(j=0;j<N2;j++){
        t=Elem(mt,i,j);
        if( sm > t) sm=t, ii=i, ji=j;
      }
    }
    match[n][0]=ii; match[n][1]=ji;
    w[n][0]=x1[ii][0]; w[n][1]=x1[ii][1];
    w[n][2]=x2[ji][0]; w[n][3]=x2[ji][1]; n++;
    for(il=0;il<N1;il++) Elem(mt,il,ji) = INFINITY;
    for(jl=0;jl<N2;jl++) Elem(mt,ii,jl) = INFINITY;
  }
}
    
int RANSAC(int w[][4],int max,double m[3][3]){
  #define MAX 100
  #define __rdtsc() ({ long long a,d; asm volatile ("rdtsc":"=a"(a),"=d"(d)); d<<32|a; })
  int i, rndAry[MAX];
  for(i=0;i<MAX;i++) rndAry[i]=i;
  srand48(__rdtsc());

  for(i=0;i<4;i++){
    int j, t;
    j=(int)(drand48()*MAX); // 乱数関数は stdlib.h で宣言されている.
    t=rndAry[i]; rndAry[i]=rndAry[j]; rndAry[j]=t;
  }
  
  int a=rndAry[0], b=rndAry[1], c=rndAry[2], d=rndAry[3];
  double xy[][2]={
    w[a][0], w[a][1],
    w[b][0], w[b][1],
    w[c][0], w[c][1],
    w[d][0], w[d][1]
  },uv[][2]={
    w[a][2], w[a][3],
    w[b][2], w[b][3],
    w[c][2], w[c][3],
    w[d][2], w[d][3]
  };

  Matrix *cmA, *vt, *mtR, *tmp;
  double z=1;

  cmA=MatrixAlloc(8,8);
  vt=MatrixAlloc(1,8);

  // create A (col-major)
  for(i=0;i<4;i++){
    cmA->data[cmA->W*0+(i*2  )]=z*xy[i][0];
    cmA->data[cmA->W*1+(i*2  )]=z*xy[i][1];
    cmA->data[cmA->W*2+(i*2  )]=z*z;
    cmA->data[cmA->W*3+(i*2  )]=0;
    cmA->data[cmA->W*4+(i*2  )]=0;
    cmA->data[cmA->W*5+(i*2  )]=0;
    cmA->data[cmA->W*6+(i*2  )]=-xy[i][0]*uv[i][0];
    cmA->data[cmA->W*7+(i*2  )]=-xy[i][1]*uv[i][0];
    cmA->data[cmA->W*0+(i*2+1)]=0;
    cmA->data[cmA->W*1+(i*2+1)]=0;
    cmA->data[cmA->W*2+(i*2+1)]=0;
    cmA->data[cmA->W*3+(i*2+1)]=z*xy[i][0];
    cmA->data[cmA->W*4+(i*2+1)]=z*xy[i][1];
    cmA->data[cmA->W*5+(i*2+1)]=z*z;
    cmA->data[cmA->W*6+(i*2+1)]=-xy[i][0]*uv[i][1];
    cmA->data[cmA->W*7+(i*2+1)]=-xy[i][1]*uv[i][1];
    vt->data[i*2  ]=z*uv[i][0];
    vt->data[i*2+1]=z*uv[i][1];
  }

  // solve Least-squares equation
  mtR=MatrixAlloc(8,8);
  MatrixQRDecompColMajor(mtR,cmA);
  tmp=MatrixAlloc(1,8);
  MatrixMultT(tmp,vt,cmA);
  MatrixSimeqLr(tmp,mtR);
  int j=0;
  for(i=0;i<20;i++){
    double x=w[i][0], y=w[i][1],
           u=w[i][2], v=w[i][3],
           du,dv;
    du = u-(tmp->data[0]*x+tmp->data[1]*y+tmp->data[2])/(tmp->data[6]*x+tmp->data[7]*y+1);
    dv = v-(tmp->data[3]*x+tmp->data[4]*y+tmp->data[5])/(tmp->data[6]*x+tmp->data[7]*y+1);
    if(du*du+dv*dv<10) j++;
  }
  //printf("The number of true is %d\n",j);
  if(j>max){
    m[0][0]=tmp->data[0]; m[0][1]=tmp->data[1]; m[0][2]=tmp->data[2];
    m[1][0]=tmp->data[3]; m[1][1]=tmp->data[4]; m[1][2]=tmp->data[5];
    m[2][0]=tmp->data[6]; m[2][1]=tmp->data[7]; m[2][2]=1;
    return j;
  }
  return max;
}

#define COUNT 100000

void greedy(Image*im,Image*im2,int x1[][2],int x2[][2],double m[3][3]){
  Matrix *mt;
  int w[999][4];
  int N1=FeaturePoint, N2=FeaturePoint;

  mt=MatrixAlloc(N1,N2);
  calcSSDtable(mt,im,x1,N1,im2,x2,N2);

  int nm,match[999][2];
  nm=matchMethod(w,match,mt,im,x1,N1,im2,x2,N2); // 特徴点の対応付け

  int i,n=0;
  for(i=0;i<COUNT;i++) n=RANSAC(w,n,m);
}
