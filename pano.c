// gcc -O pano.c && ./a.out 0.jpg 1.jpg && display out.ppm
#define FeaturePoint 30

#include"image.h"
#include"image.c"
#include"TKfilter.c"
#include"greedy.c"

void mult33(double m[3][3],double m1[3][3],double m2[3][3]){
  int i,j,k;
  for(i=0;i<3;i++){
    for(j=0;j<3;j++){
      m[i][j]=0;
      for(k=0;k<3;k++){
        m[i][j] += m1[i][k]*m2[k][j];
      }
    }
  }
}

void ArrayCopy_2(int a[][2],int b[][2],int m){
  int i,j;
  for(i=0;i<m;i++){
    for(j=0;j<2;j++) a[i][j]=b[i][j];
  }
}

void ArrayCopy33(double a[3][3],double b[3][3]){
  int i,j;
  for(i=0;i<3;i++){
    for(j=0;j<3;j++) a[i][j]=b[i][j];
  }
}

void ImageImageProjectionAlpha(Image*id,Image*is,double a[3][3],double alpha){
  int x,y,u,v;
  double r;
  for(y=0;y<id->H;y++) for(x=0;x<id->W;x++){
    r = 1 / (a[2][0]*x+a[2][1]*y+a[2][2]);
    u = r * (a[0][0]*x+a[0][1]*y+a[0][2]);
    v = r * (a[1][0]*x+a[1][1]*y+a[1][2]);
    if( isInsideImage(is,u,v) ){
      id->data[(y*id->W+x)*3+0] += is->data[(v*is->W+u)*3+0]*alpha,
      id->data[(y*id->W+x)*3+1] += is->data[(v*is->W+u)*3+1]*alpha,
      id->data[(y*id->W+x)*3+2] += is->data[(v*is->W+u)*3+2]*alpha;
    }
  }
}

int main(int ac,char**av){
  int i, x1[FeaturePoint][2], x2[FeaturePoint][2];
  Image *im,*im2,*id;
  double alpha=1.00/(ac-1.00);

  if(ac<3) return 1;

  id=ImageAlloc(1024,768);
  ImageClear(id);

  im=ImageRead(av[2]);
  im2=ImageRead(av[1]);

  int to0[FeaturePoint][2]={0};

  double m0d[][3]={
    1,0,-100,
    0,1,-100,
    -.0005,-.0005,1
  },m10[3][3],m1d[3][3];

  TKfilter(im,x1); TKfilter(im2,x2);
  greedy(im,im2,x1,x2,m10);
  ImageImageProjectionAlpha(id,im,m0d,alpha);
  mult33(m1d,m10,m0d);
  ImageImageProjectionAlpha(id,im2,m1d,alpha);
  
  for(i=0;i<ac-3;i++){
    ArrayCopy_2(x2,to0,FeaturePoint);
    im2=ImageRead(av[i+3]);
    TKfilter(im2,x2);
    greedy(im,im2,x1,x2,m10);
    mult33(m1d,m10,m0d);
    ArrayCopy33(m0d,m1d);
    ImageImageProjectionAlpha(id,im2,m1d,alpha);
    im=ImageRead(av[i+3]);
    ArrayCopy_2(x1,x2,FeaturePoint);
  }
    

  ImageWrite("out.ppm",id);

  return 0;
}
